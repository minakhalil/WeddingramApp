import { Component } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';
import { NgZone} from '@angular/core';
import {Categories} from '../categories/categories';
import { OnInit } from '@angular/core';
import {DataService} from  '../../app/services/dataService';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

width:number;
errorWithServer:boolean = false;
isLoading:boolean;
loader:any;
adsUser:any;
annUrl:any;

  constructor(public navCtrl: NavController,private ngZone:NgZone,loadingController: LoadingController,public dataService:DataService ) {
    console.log("Home init");
    this.isLoading = true;    
    this.adsUser = "";
    this.annUrl = "";
    this.loader = loadingController.create({
      content: "Loading app"
    });  
this.loader.present();
  window.onresize = (e) =>
     {
         //ngZone.run will help to run change detection
         this.ngZone.run(() => {
            this.width=(window.innerWidth)/100;
         });
     };


  }

  navToCategories(){
    this.navCtrl.push(Categories);
  }

  ngOnInit():void{

       this.dataService.getHomePage().subscribe(
            data => {
                    this.adsUser = data.ads;
                    this.annUrl = data.announcment[0].url;
               
                    this.isLoading = false;
                    this.errorWithServer = false;
                    this.loader.dismiss();
                    },
            err => {
                    console.log("err");
                    this.errorWithServer = true;
                    this.loader.dismiss();
                    },
            () => {}
            
            
        );

    this.width=(window.innerWidth)/100;
  }
}
