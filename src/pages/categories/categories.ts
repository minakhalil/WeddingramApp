import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Packages} from '../packages/packages';
import {DataService} from  '../../app/services/dataService';
import { OnInit } from '@angular/core';
/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class Categories {
  fillData:any;
  isLoading:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public dataService:DataService) {
   this.fillData = "";
   this.isLoading=true;
  }

  navToPackages(name,id){
    this.navCtrl.push(Packages,{catName: name,catId:id});
  }

     ngOnInit():void{
       console.log("Package init");
       this.dataService.getCategories().subscribe(
            data => {
                    this.fillData = data;
                   
                     this.isLoading=false;
                    },
            err => {
                    console.log("err");
               
                    this.isLoading=false;
                    },
            () => {}
            
            
        );

    
  }

}
