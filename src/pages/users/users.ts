import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { OnInit } from '@angular/core';
import {DataService} from  '../../app/services/dataService';

/**
 * Generated class for the Users page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-users',
  templateUrl: 'users.html',
})
export class Users implements OnInit{
 public search:any;
 isLoading:boolean;
 public searchColor:any;
 public searchKeyword:any;
 public passedData:any;
 public fillData:Object[];
  public filterData:any;
  public info:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public dataService:DataService) {
    
    this.search = false;
    this.isLoading = true;
    this.fillData = [];
    this.filterData = "";
    this.searchKeyword = "";
    this.info = "";
    this.searchColor = null;
    this.passedData = navParams.get("passedData");
  }

  toggleSearch(){
   
    this.search = !this.search;
    this.searchColor=(this.search)?'danger':null;
  }

    ngOnInit():void{
      
      console.log("Users init");
       this.dataService.getUsers(this.passedData.catId,this.passedData.packId).subscribe(
            data => {
                    this.fillData = data.users;
                    this.filterData = this.fillData;
                    console.log("data",this.passedData.catId,this.passedData.packId,data.users);
                     this.isLoading=false;
                     this.info = data.package.tip;
                    },
            err => {
                    console.log("err");
               
                    this.isLoading=false;
                    },
            () => {}
            
            
        );

    
  }

  onInput(ev) {
    console.log(this.filterData);
    
    // // // // Reset items back to all of the items
    this.filterData = this.fillData
    //set val to the value of the searchbar
     let val = this.searchKeyword; //this.searchKeyword

    // // // // // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.filterData = this.filterData.filter((item) => {
       return (item.show_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
     }
  }

  onCancel(e){

  }
 

}
