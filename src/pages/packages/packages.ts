import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Users} from '../users/users';
import { OnInit } from '@angular/core';
import {DataService} from  '../../app/services/dataService';
/**
 * Generated class for the Packages page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-packages',
  templateUrl: 'packages.html',
})
export class Packages implements OnInit {
isLoading:boolean;
fillDataPackages:Object[];
public catName:any;
public catId:any;

  constructor(public navCtrl: NavController, public navParams: NavParams ,public dataService:DataService) {
   
    this.isLoading = true;
    this.fillDataPackages = [];
    this.catName = navParams.get("catName");
    this.catId = navParams.get("catId");
  }

   navToUsers(name,id){
    this.navCtrl.push(Users,{passedData:{catName:this.catName,packName:name,catId:this.catId,packId:id}});
  }

    ngOnInit():void{
       console.log("Package init");
       this.dataService.getPackage(this.catId).subscribe(
            data => {
                    this.fillDataPackages = data;
                   
                     this.isLoading=false;
                    },
            err => {
                    console.log("err");
               
                    this.isLoading=false;
                    },
            () => {}
            
            
        );

    
  }

}
