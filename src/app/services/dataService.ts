import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
 
 const SERVER_URL = "http://14400e87.ngrok.io/api/v1";
 
 
@Injectable()
export class DataService {
        http: any;
       
 
    constructor (http:Http) {
        this.http = http;
    }
 
    getHomePage() {
        return this.http.get(SERVER_URL +"/home")
            .map(res => res.json())
            .catch(this.handleError);
    }
    
    getCategories() {
        return this.http.get(SERVER_URL+"/categories")
            .map(res => res.json())
            .catch(this.handleError);
    }

    getPackage(category) {
        return this.http.get(SERVER_URL+"/"+category+"/packages")
            .map(res => res.json())
            .catch(this.handleError);
    }

    getUsers(category,packageName) {
        return this.http.get(SERVER_URL+"/"+category+"/"+packageName)
            .map(res => res.json())
            .catch(this.handleError);
    }
 
   
 
    handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
 
}